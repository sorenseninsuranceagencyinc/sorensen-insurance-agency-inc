Sorensen Insurance is a home and auto insurance agency in Utah that only represents preferred carriers. This means that if you have good credit and a good driving record, you can be placed in a pool with others that are good as well. This saves you a lot of money on premiums.

 Address: 123 S Main St, Suite 8, Heber City, UT 84032, USA
 
 Phone: 435-654-6664

